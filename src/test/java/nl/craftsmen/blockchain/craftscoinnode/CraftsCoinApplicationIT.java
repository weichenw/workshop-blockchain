package nl.craftsmen.blockchain.craftscoinnode;

import nl.craftsmen.blockchain.craftscoinnode.util.InstanceInfo;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CraftsCoinApplicationIT {


    @Autowired
    private InstanceInfo instanceInfo;

    @Test
    public void allSpringBootWiringIsOk() {
    }

    @After
    public void cleanup() throws IOException {
        String node = instanceInfo.getNode();
        Files.delete(Paths.get(System.getProperty("user.dir"), normalize(node) + "-blockchain.json"));
        Files.delete(Paths.get(System.getProperty("user.dir"), normalize(node) + "-keys.json"));
        Files.delete(Paths.get(System.getProperty("user.dir"), normalize(node) + "-peers.json"));
    }

    private String normalize(String node) {
        return node.replace(":", "").replace(".", "");
    }

}
