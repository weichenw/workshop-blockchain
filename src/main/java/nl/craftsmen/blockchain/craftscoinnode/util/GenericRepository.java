package nl.craftsmen.blockchain.craftscoinnode.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import nl.craftsmen.blockchain.craftscoinnode.network.Network;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

@Repository
public class GenericRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(GenericRepository.class);

    private final InstanceInfo instanceInfo;

    public GenericRepository(InstanceInfo instanceInfo) {
        this.instanceInfo = instanceInfo;
    }

    public <T> Optional<T> load(Class<T> requiredType) {
        File file = createFileForThisNode(requiredType.getSimpleName().toLowerCase());
        LOGGER.info("try to access file: {}", file);
        if (file.exists()) {
            LOGGER.info("File {} found, reading object of type: {} ", file, requiredType);
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                T t = objectMapper.readValue(file, requiredType);
                LOGGER.info("sucessully parsed object {}", t);
                return Optional.of(t);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            LOGGER.info("File {} not found.");
            return Optional.empty();
        }
    }

    private String getFileSuffix(Object object) {
        return object.getClass().getSimpleName().toLowerCase();
    }

    public void save(Object object) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
            File file = createFileForThisNode(getFileSuffix(object));
            objectMapper.writeValue(file, object);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private File createFileForThisNode(String suffix) {
        return new File(System.getProperty("user.dir"), createKeyPairFileName(instanceInfo.getNode(), suffix));
    }

    private String createKeyPairFileName(String node, String suffix) {
        return node.replace(".", "").replace(":", "") + "-" + suffix + ".json";
    }
}
