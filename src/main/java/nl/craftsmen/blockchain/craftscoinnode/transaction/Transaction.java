package nl.craftsmen.blockchain.craftscoinnode.transaction;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.math.BigDecimal;
import java.util.UUID;

@JsonPropertyOrder(alphabetic = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, isGetterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
public class Transaction {

    //TODO insert fields

    //default public constructor, needed for json deserialization
    @SuppressWarnings({"unused", "WeakerAccess"})
    public Transaction() {

    }

    //TODO insert constructor

    //TODO getters and setters

    //TODO equals based on id, hashcode based on id, toString
}
