package nl.craftsmen.blockchain.craftscoinnode.transaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Manages pending transactions.
 */
@Component
public class TransactionPool {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionPool.class);

    /**
     * Add a new transaction to this pool.
     * @param transaction the transaction.
     */
    public void addTransaction(Transaction transaction) {
        //TODO implement me
    }

    /**
     * Removes all transactions from the pool.
     */
    public void clearTransactions() {
        //TODO implement me
    }

    /**
     * Removes all transactions from the pool that are in the supplied transactions list.
     * @param transactions the supplied transactions list.
     */
    public void clearTransactions(List<Transaction> transactions) {
        //TODO implement me
    }

    /**
     * Gets all transactions from the pool in an unmodifiable list.
     * @return sll transactions from the pool.
     */
    public Set<Transaction> getAllTransactions() {
        //TODO implement me
        return null;
    }
}
