package nl.craftsmen.blockchain.craftscoinnode;

import nl.craftsmen.blockchain.craftscoinnode.blockchain.BlockchainService;
import nl.craftsmen.blockchain.craftscoinnode.network.Network;
import nl.craftsmen.blockchain.craftscoinnode.transaction.SignatureService;
import nl.craftsmen.blockchain.craftscoinnode.util.InstanceInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
class ApplicationStartedListener implements ApplicationListener<ApplicationStartedEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationStartedListener.class);

    private final Environment environment;
    private final InstanceInfo instanceInfo;
    private final Network network;
    private final BlockchainService blockchainService;
    private final SignatureService signatureService;
    private final StartedInterceptor startedInterceptor;

    @Autowired
    public ApplicationStartedListener(Environment environment, InstanceInfo instanceInfo, Network network, BlockchainService blockchainService, SignatureService signatureService, StartedInterceptor startedInterceptor) {
        this.environment = environment;
        this.instanceInfo = instanceInfo;
        this.network = network;
        this.blockchainService = blockchainService;
        this.signatureService = signatureService;
        this.startedInterceptor = startedInterceptor;
    }

    @Override
    public void onApplicationEvent(ApplicationStartedEvent event) {
        LOGGER.info("======== Application started ==============");
        Integer port = environment.getProperty("local.server.port", Integer.class);
        LOGGER.info("running port is: " + port);
        instanceInfo.setPort(port);
        network.init();
        network.connectToNetwork();
        blockchainService.init();
        signatureService.init();
        startedInterceptor.open();
    }
}
