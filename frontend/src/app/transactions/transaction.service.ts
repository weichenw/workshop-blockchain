import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { MessageService } from '../messages/message.service';

import { Transaction, TransactionResult } from '../api';

import 'rxjs/add/operator/do';

@Injectable()
export class TransactionService {
  constructor(private http: HttpClient, private messageService: MessageService) {}

  getTransactions(): Observable<Transaction[]> {
    this.log('Fetching pending transactions...');

    return this.http.get<Transaction[]>('/api/pendingtransactions')
      .do(transactions => this.log(`Got ${transactions.length} transactions`));
  }

  save(transaction: Transaction): Observable<TransactionResult> {
    this.log('Sending new transaction');

    return this.http.post<TransactionResult>('/api/createtransaction', transaction)
      .do(result => this.log(result.message));
  }

  private log(message: string): void {
    this.messageService.log(message);
  }
}
