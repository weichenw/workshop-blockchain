import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/switchMap';

import { Transaction } from '../../api';
import { TransactionService } from '../transaction.service';

@Component({
  selector: 'app-pending-transactions',
  templateUrl: './pending-transactions.component.html'
})
export class PendingTransactionsComponent implements OnInit, OnDestroy {
  transactions: Transaction[] = [];

  private subscription: Subscription;

  constructor(private transactionService: TransactionService) {}

  ngOnInit(): void {
    this.subscription = Observable.timer(0, 5000)
      .switchMap(() => this.transactionService.getTransactions())
      .subscribe(transactions => this.transactions = transactions);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  trackByFn(index: number, transaction: Transaction): string {
    return transaction.id;
  }
}
