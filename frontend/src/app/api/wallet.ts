import { Transaction } from './transaction';

export interface Wallet {
  balance: number;
  minedTransactions: Transaction[];
  unconfirmedTransactions: Transaction[];
}
