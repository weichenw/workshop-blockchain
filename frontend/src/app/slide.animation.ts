import { animate, animation, query, stagger, style } from '@angular/animations';

export const slideAnimation = animation([
  query(':enter', [
    style({height: 0, paddingTop: 0, paddingBottom: 0, opacity: 0, marginTop: 0, marginBottom: 0}),
    stagger(100, [
      animate(200),
    ]),
  ], {optional: true}),
  query(':leave', [
    animate(200, style({height: 0, paddingTop: 0, paddingBottom: 0, opacity: 0, marginTop: 0, marginBottom: 0})),
  ], {optional: true}),
]);
