import { Component, OnInit } from '@angular/core';

import { WalletService } from './wallet.service';

@Component({
  selector: 'app-wallets',
  templateUrl: './wallets.component.html',
  styleUrls: ['./wallets.component.scss']
})
export class WalletsComponent implements OnInit {
  name = '';
  lastQuery: string;

  constructor(private walletService: WalletService) {}

  ngOnInit(): void {
    this.lastQuery = this.walletService.lastQuery;
  }

  search(): void {
    this.lastQuery = this.name;
    this.walletService.lastQuery = this.lastQuery;

    this.name = '';
  }
}
